'use strict';

var async = require('async');
var Booking = require('./booking.model');
var Class = require('../class/class.model');
var Stripe = require('../../config/Stripe');

var validationError = function(res, err) {
    return res.json(500, err);
};

/**
 * Get list of cart classes
 */
exports.index = function(req, res) {
    Booking.
    find({is_paid: 'PAID'}).
    populate('class student').
    exec(function (err, carts) {
        if (err) return validationError(res, err);
        if(err) return res.send(500, err);
        return res.status(200).send(carts) ;
    });
};

/**
 * Get list of cart classes
 */
exports.myAllBookedClass = function(req, res) {
    Booking.
    find({student: req.user._id}).
    populate('class').
    exec(function (err, carts) {
        if (err) return validationError(res, err);
        if(err) return res.send(500, err);
        return res.status(200).send(carts) ;
    });
};

/**
 * Get list of my booking classes
 */
exports.myCart = function(req, res) {
    Booking.
    find({student: req.user._id, is_paid: 'CART'}).
    populate('class'). // only return the Persons name
    exec(function (err, carts) {
        if (err) return validationError(res, err);
        if(err) return res.send(500, err);
        return res.status(200).send(carts) ;
    });
};


/**
 * Get list of my booking classes
 */
exports.myBooking = function(req, res) {
    Booking.
    find({student: req.user._id, is_paid: 'PAID'}).
    populate('class'). // only return the Persons name
    exec(function (err, carts) {
        if (err) return validationError(res, err);
        if(err) return res.send(500, err);
        return res.status(200).send(carts) ;
    });
};

/**
 * Get list of my booking classes
 */
exports.getListAllCharge = function(req, res) {
    Stripe.listAllCharge()
        .then((charges) => {
            return res.status(200).send(charges) ;
        }, (err) => {
            return validationError(res, err)
        });
};

/**
 * Get list of my booking classes
 */
exports.createBookingClass = function(req, res) {

    Stripe.createCharge(req.body.total, req.body.paymentToken, req.user.email, req.body.bookingIds)
        .then((charge) => {
            Booking.update(
                {_id: { $in: req.body.bookingIds}},
                { $set: {"is_paid": 'PAID'}},
                { multi: true },
                function (err, carts) {
                    if (err) return validationError(res, err);
                    if(err) return res.send(500, err);
                    return Class.update(
                        {_id: { $in: req.body.groupIds}},
                        { $push: { students: req.user._id } },
                        { multi: true },
                        function (err, classes) {
                            if (err) return validationError(res, err);
                            if(err) return res.send(500, err);

                            return res.status(200).send(carts) ;
                        });
                });
        }, (err) => {
            return validationError(res, err)
        });
};

/**
 * Get list of my booking classes
 */
exports.syncBooking = function(req, res) {
    async.each(req.body.cart, function(cart, callback) {

        Booking.findOne({class: cart.class._id, student: req.user._id}, function(err, booking) {
            if(err) return res.send(500, err);
            if(booking) callback();
            else {
                Class.findOne({_id: cart.class._id, students: { "$in": req.user._id }}, function(err, classObj) {
                    if(err) return res.send(500, err);
                    if(classObj) callback();
                    else {
                        let bookedClass = {
                            student: req.user._id,
                            class: cart.class._id,
                            fees: cart.fees,
                            lessons: cart.lessons,
                            describe: cart.describe
                        };

                        Booking.create(bookedClass, function(err, bookedclass) {
                            if(err) { return validationError(res, err); }
                            callback();
                        });
                    }
                });
            }
        });
    }, function(err) {
        if(err) { return validationError(res, err); }
        return res.json(200);
    });
};


/**
 * Create new booking class
 */
exports.create = function (req, res) {

    let bookedClass = {
        student: req.user._id,
        class: req.body.class,
        fees: req.body.fees,
        lessons: req.body.lessons,
        describe: req.body.describe
    };

    Booking.create(bookedClass, function(err, bookedclass) {
        if(err) { return validationError(res, err); }
        return res.json(201, bookedclass);
    });
};

/**
 * Deletes a booking class
 */
exports.destroy = function(req, res) {
    Booking.findByIdAndRemove(req.params.id, function(err) {
        if(err) return res.send(500, err);
        return res.send(204);
    });
};