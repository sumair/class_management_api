'use strict';

// Development specific configuration
// ==================================
module.exports = {
    // MongoDB connection options
    mongo: {
        uri: 'mongodb://localhost/class-management'
    },
    emailUrl: 'http://localhost:3000',
    seedDB: true
};
