'use strict';

var express = require('express');
var controller = require('./booking.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/me', auth.isAuthenticated(), controller.myAllBookedClass);
router.get('/cart', auth.isAuthenticated(), controller.myCart);
router.get('/booking', auth.isAuthenticated(), controller.myBooking);
router.get('/allCharges', auth.isAuthenticated(), controller.getListAllCharge);
router.post('/booking', auth.isAuthenticated(), controller.createBookingClass);
router.post('/sync', auth.isAuthenticated(), controller.syncBooking);
router.post('/', auth.isAuthenticated(), controller.create);
router.delete('/:id', auth.hasRole('student'), controller.destroy);

module.exports = router;