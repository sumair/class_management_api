'use strict';

var Class = require('./class.model');
var async = require('async');

var validationError = function(res, err) {
    return res.json(500, err);
};

/**
 * Get list of classes
 * restriction: 'teacher'
 */
exports.index = function(req, res) {
    let search = {};

    if(req.user.role === 'teacher') {
        search.teacher = req.user._id;
        search.status = 'ACTIVE';
    }

    Class.find(search, function (err, classes) {
        if(err) return res.send(500, err);
        res.json(200, classes);
    });
};

/**
 * Get list of classes events
 */
exports.getEventClasses = function(req, res) {
    let search = { status: 'ACTIVE' };

    if(req.user.role === 'teacher') {
        search.teacher = req.user._id;
    }
    else if(req.user.role === 'student') {
        search.students = { "$in": req.user._id };
    }

    Class.
    find(search).
    populate('students', 'email first_name last_name phone gender school_college exam_board year_group current_grade customer_id'). // only return the Persons name
    exec(function (err, classes) {
        if(err) return res.send(500, err);
        res.json(200, classes);
    });
};

/**
 * Create new classes
 */
exports.create = function (req, res) {

    Class.insertMany(req.body, function(err, classObj) {
        if(err) { return validationError(res, err); }
        return res.json(201, classObj);
    });
};

/**
 * Create new classes
 */
exports.searchClass = function (req, res) {

    let search = {
        type: req.body.studyMethod,
        year_group: req.body.academicYear,
        awarding_bodies: {$in : req.body.awardingBodies},
        month: {$in : req.body.selectedMonths}
    };

    if(req.user && req.user._id) search.students = { "$nin": req.user._id };
    if(req.body.subject) search.subject = req.body.subject;

    async.parallel({
        searchResults: function(callback) {
            Class.find(search, function(err, classes) {
                if(err) callback(err, null);
                else callback(null, classes);
            });
        },
        revisionResults: function(callback) {
            if(req.body.academicYear === "11") callback(null, []);
            else {
                if(req.body.academicYear === "12") search.year_group = "11";
                if(req.body.academicYear === "13") search.year_group = { $in: ["11", "12"] };
                Class.find(search, function(err, classes) {
                    if(err) callback(err, null);
                    else callback(null, classes);
                });
            }
        }
    }, function(err, results) {
        if(err) { return validationError(res, err); }
        return res.json(200, results);
    });

};


/**
 * get a class
 */
exports.show = function (req, res, next) {
    var userId = req.params.id;

    Class.findById(userId, function (err, classObj) {
        if (err) return next(err);
        res.json(classObj);
    });
};

/**
 * Edit class
 */
exports.editClass = function (req, res) {

    let classObj = {
        year_group: req.body.year_group,
        subject: req.body.subject,
        awarding_bodies: req.body.awarding_bodies,
        month: req.body.month,
        date: req.body.date,
        startTime: req.body.startTime,
        endTime: req.body.endTime,
        seats: req.body.seats
    };

    Class.findByIdAndUpdate(req.params.id,
        {
            $set: classObj
        },
        {new: true},
        function(err, result){
            if (err) return validationError(res, err);
            return res.json(200, result);
        });
};

/**
 * Cancel a class
 * restriction: 'admin'
 */
exports.cancel = function(req, res) {
    var classId = req.params.id;

    Class.findById(classId, function (err, classObj) {
        classObj.status = 'CANCEL';
        classObj.save(function(err) {
            if (err) return validationError(res, err);
            res.send(200);
        });
    });
};


/**
 * Active a class
 * restriction: 'admin'
 */
exports.active = function(req, res) {
    var classId = req.params.id;

    Class.findById(classId, function (err, classObj) {
        classObj.status = 'ACTIVE';
        classObj.save(function(err) {
            if (err) return validationError(res, err);
            res.send(200);
        });
    });
};
