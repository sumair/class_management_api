'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var UserSchema = new Schema({
    username: {
        type: String,
        lowercase: true,
        required: true
    },
    email: {
        type: String,
        lowercase: true,
        required: true
    },
    first_name: {
        type: String,
        optional: true
    },
    last_name: {
        type: String,
        optional: true
    },
    phone: {
        type: Number,
        optional: true
    },
    dob: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        lowercase: true,
        required: true
    },
    subject: {
        type: String,
        optional: true
    },
    parental_email: {
        type: String,
        lowercase: true,
        required: true
    },
    school_college: {
        type: String,
        lowercase: true,
        optional: true
    },
    exam_board: {
        type: String,
        lowercase: true,
        optional: true
    },
    year_group: {
        type: String,
        lowercase: true,
        optional: true
    },
    current_grade: {
        type: String,
        optional: true
    },
    role: {
        type: String,
        default: 'student'
    },
    address_line: {
        type: String,
        optional: true
    },
    city: {
        type: String,
        lowercase: true,
        optional: true
    },
    postcode: {
        type: String,
        lowercase: true,
        optional: true
    },
    customer_id: {
        type: String,
        unique: true,
        optional: true
    },
    hashedPassword: String,
    resetPasswordToken: {
        type: String,
        optional: true
    },
    resetPasswordExpires: {
        type: Number,
        optional: true
    },
    provider: String,
    salt: String
});

/**
 * Virtuals
 */
UserSchema
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

// Public profile information
UserSchema
    .virtual('profile')
    .get(function() {
        return {
            'username': this.username,
            'role': this.role
        };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
        return {
            '_id': this._id,
            'role': this.role
        };
    });

/**
 * Validations
 */

// Validate empty email
UserSchema
    .path('email')
    .validate(function(email) {
        return email.length;
    }, 'Email cannot be blank');

// Validate empty password
UserSchema
    .path('hashedPassword')
    .validate(function(hashedPassword) {
        return hashedPassword.length;
    }, 'Password cannot be blank');

// Validate username is not taken
UserSchema
    .path('username')
    .validate(function (value, respond) {
    return mongoose.model('User').findOne({ username: value }).exec().then(function (count) {
        return !count;
    })
        .catch(function (err) {
            throw err;
        });
}, 'The specified username is already in use.');

// Validate email is not taken
UserSchema
    .path('email')
    .validate(function (value, respond) {
        return mongoose.model('User').findOne({ email: value }).exec().then(function (count) {
            return !count;
        })
            .catch(function (err) {
                throw err;
            });
    }, 'The specified email address is already in use.');

var validatePresenceOf = function(value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function(next) {
        console.log("next: ", next);
        if (!this.isNew) return next();

        if (!validatePresenceOf(this.hashedPassword))
            next(new Error('Invalid password'));
        else
            next();
    });

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashedPassword;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function(password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('base64');
    }
};

module.exports = mongoose.model('User', UserSchema);
