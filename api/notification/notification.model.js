'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    message: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum : ['ADMIN','TEACHER'],
        default: 'ADMIN'
    },
    created: {
        type: Date,
        default: Date.now
    }
});


/**
 * Validations
 */

// Validate empty year_group
NotificationSchema
    .path('message')
    .validate(function(message) {
        return message.length;
    }, 'Message cannot be blank');

module.exports = mongoose.model('Notification', NotificationSchema);
