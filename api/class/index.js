'use strict';

var express = require('express');
var controller = require('./class.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('teacher'), controller.index);
router.get('/events', auth.isAuthenticated(), controller.getEventClasses);
router.put('/:id/cancel', auth.hasRole('admin'), controller.cancel);
router.put('/:id/active', auth.hasRole('admin'), controller.active);
router.put('/:id', auth.hasRole('teacher'), controller.editClass);
router.post('/', auth.hasRole('teacher'), controller.create);
router.post('/search', controller.searchClass);

module.exports = router;