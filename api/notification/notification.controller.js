'use strict';

var Notification = require('./notification.model');

var validationError = function(res, err) {
    return res.json(500, err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
    let search = { type: 'TEACHER'};

    if(req.user.role === 'teacher') search.type = 'ADMIN';

    Notification.find(search, function (err, notifications) {
        if(err) return res.send(500, err);
        res.json(200, notifications);
    });
};

/**
 * Creates a new student
 */
exports.create = function (req, res) {
    let notification = {
        message: req.body.message,
        type: req.user.role === 'admin' ? 'ADMIN' : 'TEACHER'
    };

    Notification.create(notification, function(err, notification) {
        if(err) { return validationError(res, err); }
        return res.json(201, notification);
    });
};
