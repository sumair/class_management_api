// var secret_key = 'sk_live_IB00i9ZCx0AWuvZrVY3WU4b3'; //live
var secret_key = 'sk_test_IFevw7z5lPt20FmwMEsJGxCL';   //test
var stripe = require('stripe')(secret_key);

exports.createCharge = function (amount, source, email) {
    return new Promise((resolve, reject) => {
        stripe.charges.create({
            "amount": amount * 100,
            "currency": "gbp",
            "source": source,
            "receipt_email": email
        }, function(err, charge) {
            console.log("err: ", err);
            console.log("charge: ", charge);
            if(err) reject(err);
            else resolve(charge);
        });
    })
};

exports.listAllCharge = function () {
    return new Promise((resolve, reject) => {
        stripe.charges.list(
            function(err, charges) {
                if(err) reject(err);
                else resolve(charges);
            }
        );
    })
};