'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/teachers', auth.hasRole('admin'), controller.getAllTeachers);
router.post('/login', controller.login);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/students', auth.hasRole('teacher'), controller.myStudents);
router.get('/students/parent_email', auth.hasRole('teacher'), controller.allStudentParents);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.post('/forgot', controller.forgotPassword);
router.post('/reset/:token', controller.resetPassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.post('/customer', auth.isAuthenticated(), controller.createCustomer);
router.post('/mail/parent', controller.sendMail);
router.post('/teacher', auth.hasRole('admin'), controller.createTeacher);
router.post('/exist/email', controller.isEmailExist);
router.post('/exist/username', controller.isUsernameExist);

module.exports = router;