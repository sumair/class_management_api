/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User = require('../api/user/user.model');
var users = [
    {
        provider: 'local',
        role: 'admin',
        username: 'admin',
        email: 'admin@admin.com',
        password: 'admin',
        first_name: 'admin',
        last_name: 'admin',
        phone: 123456789,
        dob: new Date(),
        gender: 'male',
        school_college: 'school',
        exam_board: 'karachi board',
        year_group: '11',
        current_grade: 'A+',
        address_line: 'address_line',
        city: 'town_city',
        postcode: 'postcode',
    },
    {
        provider: 'local',
        role: 'teacher',
        username: 'teacher',
        email: 'teacher@teacher.com',
        password: 'teacher',
        first_name: 'teacher',
        last_name: 'teacher',
        phone: 123456789,
        dob: new Date(),
        gender: 'male',
        school_college: 'school',
        exam_board: 'karachi board',
        year_group: '11',
        current_grade: 'A+',
        address_line: 'address_line',
        city: 'town_city',
        postcode: 'postcode',
    },
    {
        provider: 'local',
        role: 'student',
        username: 'student',
        email: 'student@student.com',
        password: 'student',
        first_name: 'student',
        last_name: 'student',
        phone: 123456789,
        dob: new Date(),
        gender: 'male',
        school_college: 'school',
        exam_board: 'karachi board',
        year_group: '11',
        current_grade: 'A+',
        address_line: 'address_line',
        city: 'town_city',
        postcode: 'postcode',
    }
];
//
// User.find({}).remove(function() {
//     User.create(users, function() {
//             console.log('finished populating users');
//         }
//     );
// });
