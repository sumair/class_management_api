'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookingSchema = new Schema({
    class: {
        type: Schema.Types.ObjectId,
        ref: 'Class'
    },
    student: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    fees: {
        type: Number,
        optional: true
    },
    lessons: [String],
    describe: {
        type: String,
        optional: true
    },
    is_paid: {
        type: String,
        enum : ['PAID','CART'],
        default: 'CART'
    }
});


module.exports = mongoose.model('Booking', BookingSchema);
