'use strict';

var User = require('./user.model');
var Class = require('../class/class.model');
var passport = require('passport');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');
var config = require('../../config/environment');

var validationError = function(res, err) {
    return res.json(422, err);
};

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'class.management123456@gmail.com',
        pass: 'ClassManagement123'
    }
});

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
    User.find({}, '-salt -hashedPassword', function (err, users) {
        if(err) return res.send(500, err);
        res.json(200, users);
    });
};

/**
 * Get list of Teachers
 * restriction: 'admin'
 */
exports.getAllTeachers = function(req, res) {
    User.find({role: 'teacher'}, '-salt -hashedPassword', function (err, teachers) {
        if(err) return res.send(500, err);
        res.json(200, teachers);
    });
};

/**
 * Creates a new student
 */
exports.create = function (req, res, next) {
    var user = new User(req.body);

    user.provider = 'local';
    user.role = 'student';
    user.save(function(err) {
        if (err) return validationError(res, err);
        var token = auth.signToken(user._id, user.role);
        res.json({ token: token });
    });
};

/**
 * Creates a new student
 */
exports.createCustomer = function (req, res) {
    User.findByIdAndUpdate(req.user._id, {
        $set: {
            customer_id: req.body.customer_id
        }
    }, function(err, doc){
        if (err) return validationError(res, err);
        res.send({success: true});
    });
};

/**
 * Creates a new teacher
 */
exports.createTeacher = function (req, res, next) {
    var user = new User(req.body);

    user.provider = 'local';
    user.role = 'teacher';
    user.save(function(err) {
        if (err) return validationError(res, err);
        res.json({message: 'Teacher created successfully'})
    });
};

/**
 * Is email exist
 */
exports.isEmailExist = function (req, res) {
    User.findOne({ email: req.body.email }).exec( function (err, docs) {
        if (err) return validationError(res, err);
        if(!docs) return res.json({ exist: false});
        res.json({ exist: true});
    });
};

/**
 * Is email exist
 */
exports.isUsernameExist = function (req, res) {
    User.findOne({ username: req.body.username }).exec( function (err, docs) {
        if (err) return validationError(res, err);
        if(!docs) return res.json({ exist: false});
        res.json({ exist: true});
    });
};


/**
 * Creates a new user
 */
exports.login = function (req, res) {
    passport.authenticate('local', function(err, user, info){
        var error = err || info;
        if (error) return res.json(401, error);
        if (!user) return res.json(404, {message: 'Something went wrong, please try again.'});
        // If a user is found
        if(user){
            var token = auth.signToken(user._id, user.role);
            res.json({token: token});
        } else {
            // If user is not found
            res.status(401).json(info);
        }
    })(req, res);
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
    var userId = req.params.id;

    User.findById(userId, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(401);
        res.json(user.profile);
    });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
    User.findByIdAndRemove(req.params.id, function(err, user) {
        if(err) return res.send(500, err);
        return res.send(204);
    });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);

    User.findById(userId, function (err, user) {
        if(user.authenticate(oldPass)) {
            user.password = newPass;
            user.save(function(err) {
                if (err) return validationError(res, err);
                res.send(200);
            });
        } else {
            res.send(403);
        }
    });
};

/**
 * forgot password
 */
exports.forgotPassword = function(req, res) {
    User.findOne({email: req.body.email}, function (err, user) {
        if (err) return validationError(res, err);
        if(!user) return res.json(404, {message: 'No account with that email address exists.'});
        let resetPasswordToken = auth.createResetHash(req.body.email);
        let resetPasswordExpires = Date.now() + 3600000; // 1 hour

        User.findOneAndUpdate({email: req.body.email}, {
            $set: {
                resetPasswordToken,
                resetPasswordExpires
            }
        }, function(err, doc){
            if (err) return validationError(res, err);
            let mailOptions = {
                from: '"Class Management" <class.management123456@gmail.com>', // sender address
                to: req.body.email, // list of receivers
                subject: 'Forgot Password Email', // Subject line
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                config.emailUrl + '/reset/' + resetPasswordToken + '\n\n' +
                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                    return res.send(500, error);
                }
                res.send({success: true});
            });
        });
    })
};


/**
 * forgot password
 */
exports.resetPassword = function(req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
        if (err) return validationError(res, err);
        if (!user) return res.send(404, 'Password reset token is invalid or has expired.');
        user.password = req.body.password;
        user.resetPasswordToken = null;
        user.resetPasswordExpires = null;
        user.save({ validateBeforeSave: false }, function(err) {
            if (err) return validationError(res, err);
            res.send(200);
        });
    });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
    var userId = req.user._id;
    User.findOne({
        _id: userId
    }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
        if (err) return next(err);
        if (!user) return res.json(401);
        res.json(user);
    });
};

/**
 * Get my students
 */
exports.myStudents = function(req, res) {

    Class.
    find({teacher: req.user._id}).
    select({ "students": 1}).
    populate('students', 'email first_name last_name phone gender school_college exam_board year_group current_grade customer_id'). // only return the Persons name
    exec(function (err, classes) {
        if(err) return res.send(500, err);
        return res.status(200).send(classes) ;
    });
};

/**
 * Get all students parents email
 */
exports.allStudentParents = function(req, res) {

    User.find({role: 'student'}, 'first_name last_name parental_email', function (err, users) {
        if (err) return validationError(res, err);
        return res.status(200).send(users) ;
    });
};


/**
 * Send mail to parent
 */
exports.sendMail = function(req, res) {

// setup e-mail data with unicode symbols
    let mailOptions = {
        from: '"Class Management" <class.management123456@gmail.com>', // sender address
        to: req.body.to, // list of receivers
        subject: req.body.subject, // Subject line
        text: req.body.message
    };

   // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return res.send(500, error);
        }
        res.status(200).send(info) ;
    });

};