'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClassSchema = new Schema({
    type: {
        type: String,
        enum : ['INDIVIDUAL','GROUP'],
        default: 'INDIVIDUAL'
    },
    year_group: {
        type: String,
        lowercase: true,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    awarding_bodies: {
        type: [String],
        optional: true
    },
    date: {
        type: Date,
        required: true
    },
    month: {
        type: String,
        required: true
    },
    startTime: {
        type: String,
        required: true
    },
    endTime: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum : ['ACTIVE','CANCEL'],
        default: 'ACTIVE'
    },
    teacher: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    seats: {
        type: Number,
        optional: true
    },
    students: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
});


/**
 * Validations
 */

// Validate empty year_group
ClassSchema
    .path('year_group')
    .validate(function(year_group) {
        return year_group.length;
    }, 'Year Group cannot be blank');

// Validate empty subject
ClassSchema
    .path('subject')
    .validate(function(subject) {
        return subject.length;
    }, 'Subject cannot be blank');


module.exports = mongoose.model('Class', ClassSchema);
